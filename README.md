## Refcard03

**Description**
Refcard03 is a joke-site, or in other words a site where you can find jokes from a database.

**Prerequisites**
- java version 11
- latest version of Maven
- basic knowledge regarding Docker
- basic knowledge regarding GitLab (CI/CD)
- basic knowledge regarding AWS (ECR)
    
**Installation**
Step-by-Step Installation kann man mit folgenden Schritten durchführen:

- Fork this repository
 - 
    `git clone https://gitlab.com/[your username]/architecture-ref-card-03-new/-/jobs/4424292470`
- 
   Locally run  `docker build -t ref-card-03 .` in your terminal to build the image.
 - 
    Run `docker run -p [your port]:8080 ref-card-03` to run the image
 - 
    you can now run localhost:[your port]
- If you choose to make any edits, remember to commit and push your changes.
 
**GitLab CI/CD**
With 
-  Open your GitLab repository in your web browser.
-  Navigate to "Settings" and click on "CI/CD".
-  Scroll down to find the "Runners" section and click on "Expand".
-  Copy the provided registration token.
-  Open your terminal or command prompt.
-  Run the command `gitlab-runner register` in the terminal.
-  When prompted, paste the registration token that you copied earlier.
-  Enter a description for the GitLab Runner.
-  Optionally, add any Tags you want
-  Choose the executor you want to use. For example, if you want to use Docker, select "docker" as the executor.
-  Enter the default Docker image that should be used for running jobs. This image will be used as the base for creating job containers.

**ECR & ECS in AWS**
Follow these steps in ECS:
1. Log in to AWS and navigate to ECS and click on "Create ECS Cluster".
2. Name your ECS. The name will later be used in the Enviroment Variables in GitLab.
3. Click on "Create" to finish creating the ECS cluster.
4. Click on your cluster and scroll down to services.
5. Click the "Create" button & configure the service.
6. Give it an appropriate name, that will again be used in the Enviroment Variables in GitLab.

Follow these steps in ECR:
1.  In AWS navigate to ECR and click on "Create repository".
2. Give it an appropriate name, that will again be used in the Enviroment Variables in GitLab.

**Enviroment Variables to GitLab CI**
1.  AWS_ACCESS_KEY_ID (AWS Details)
2. AWS_SECRET_ACCESS_KEY (AWS Details)
3. AWS_SESSION_TOKEN (AWS Details)
4.  AWS_DEFAULT_REGION (AWS example: us-east-1)
5. CI_AWS_ECR_REGISTRY (url of the Registry)
6.  CI_AWS_ECR_REPOSITORY_NAME (Repository name - ECR)
7. CI_AWS_ECS_CLUSTER_NAME (Cluster name in ECS)
8. CI_AWS_ECS_SERVICE_NAME (Service name in ECS)

9. Run the GitLab pipeline



**Author / Contact**
E-Mail: aakash.sethi@lernende.bbw.ch
GitHub Link: https://gitlab.com/aakash.sethi/architecture-ref-card-03-new



 

    








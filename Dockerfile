FROM maven:3-openjdk-11-slim

COPY src /src

COPY pom.xml /

RUN mvn -f pom.xml clean package

RUN mv /target/*.jar app.jar

ENV DB_URL=$DB_URL

ENV DB_USERNAME=$DB_USERNAME

ENV DB_PASSWORD=$DB_PASSWORD

ENTRYPOINT ["java","-jar","/app.jar", "--DB_URI=${DB_URL}", "--DB_USER=${DB_USERNAME}", "--DB_PASS=${DB_PASSWORD}"]



